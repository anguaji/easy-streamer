// audio player object

function AudioPlayer(_audioPlayer, _beep) {
	this.track = _audioPlayer[0];
	this.beep = _beep[0];
	
	this.track.volume = 1;
	this.beep.volume = 0.5;

	this.audioIsPlaying = false;
	this.audioPlayerReset = true;
	this.state = "Start"; // Start, Loading, Playing, Paused, Stopped
    this.duration = "0:00";
    this.buffered = 0;
    this.bufferedPercent = 0;

    this.bufferThreshold = _audioPlayer.data("threshold"); // percentage to force streaming
    this.bufferIncrement = _audioPlayer.data("increment"); // seek increment
    this.bufferIntervalTime = _audioPlayer.data("interval"); // how often to poll buffering
    this.localCheck = _audioPlayer.data("local");

    console.log("Audio buffer threshold: " + this.bufferThreshold + "%");
    console.log("Audio buffer increment: " + this.bufferIncrement + "ms");
    console.log("Audio buffer interval: " + this.bufferIntervalTime + "ms");
    console.log("Audio failsafe loop escape: " + this.localCheck + " times");

    this.updateDuration = function(dur) {
        this.duration = dur;
        //console.log("Duration set to: " + this.duration);
    }

    this.updateBuffering = function(b, bP) {
        this.buffered = b;
        this.bufferedPercent = bP;
        //console.log("bufferedPercent set to: " + this.bufferedPercent);
    }

	this.setLoading = function() {
		this.state = "Loading";
	}

	this.isLoading = function() {
		// return loading status
		return (this.state == "Loading");
	}
	
	this.stop = function() {
		this.track.pause();
		this.audioIsPlaying = false;
		this.track.currentTime = 0;
		this.state = "Stopped";
	}

	this.pause = function() {
		this.track.pause();
		this.audioIsPlaying = false;
		this.state = "Paused";
	}

	this.play = function() {
		this.track.play();
		this.audioIsPlaying = true;
		this.state = "Playing";
	}

	this.scrub = function(amount) {
		var t = this.track.currentTime;
	    t += amount;

	    if(t < 0) 
	        t = 0;
	    else if(t > this.track.duration)
	        t = this.track.duration - 10;

	    this.track.currentTime = t;
	}

    this.goto = function(percentage) {

        var t = (percentage / 100) * this.track.duration;;
        
        if(t < 0) 
            t = 0;
        else if(t > this.track.duration)
            t = this.track.duration - 10;

        this.track.currentTime = t;
    }

	this.getCurrentPos = function() {
		return (this.track.currentTime / this.track.duration);
	}

	this.playBeep = function() {
		this.beep.play();
	}

    this.loadTrack = function(newTrack) {
		this.reset();
        this.buffered = 0;
        this.bufferedPercent = 0;
        this.track.src = newTrack;
        this.track.load();
        //console.log("New track: " + newTrack);
		this.track.play();
        this.track.pause();
        this.state = "Loading";
	}

	this.reset = function() {
		this.stop();
		this.audioPlayerReset = true;
        this.state = "Start"; // start (reset), loading, loaded, playing, paused, stopped
		jq('#track-status').html("Press Play To Start");
	}

    this.bindings = function(binding) {
		switch(binding) {
			case "timeupdate":
				var secs = this.track.currentTime;
		        var progress = Math.round((secs / this.track.duration) * 100 * 100) / 100;
				
				var tcMins = parseInt(secs / 60);
		        var tcSecs = parseInt(secs - (tcMins * 60));
		        
		        if (tcSecs < 10) { tcSecs = '0' + tcSecs; }

                if(this.audioIsPlaying) {
                    jq('#progress-bar').css({'width' : (progress) + "%"});

                    if(this.state == "Start") 
                        jq('#track-status').html("Press Play To Start");
                    else
                        jq('#track-status').html(this.state + ": " + tcMins + ':' + tcSecs + " / " + this.duration);
                }
                
                break;

            case "progress":
                if(this.track.buffered.length > 0) {
                    var buffered = this.track.buffered.end(this.track.buffered.length-1);
                    var bufferedPercent = Math.round((buffered / this.track.duration) * 100);
                    this.buffered = buffered;
                    this.bufferedPercent = bufferedPercent;  
                    //console.log("(buffering: " + buffered + " (" + bufferedPercent + "%) / " + this.track.duration + ")");
                    jq('#buffer-bar').css({'width' : (bufferedPercent) + "%"});               
                }

                break;

            case "canplaythrough":
                if(this.track.src != "") {
                    //console.log('The file ' + this.track.src +  ' is loaded and ready to play!');
                }

                break;

			default:
				//console.log("Unhandled binding: " + binding);
		}
	}

}

var jq = jQuery.noConflict();

jq(document).ready(function($) {
    
    var myPlayer = $('#mainPlayer');
    var beep = $('#beep');
    
    var audioPlayer = new AudioPlayer(jq('#mainPlayer'), jq('#beep'));

	var beepInterval; // used to track beep chain
    var bufferInterval; // used to track buffering

	jq('#mainPlayer').bind({
		timeupdate: function() { audioPlayer.bindings("timeupdate"); },
		loadstart: function() { audioPlayer.bindings("loadstart"); },
		durationchange: function() { audioPlayer.bindings("durationchange"); },
		loadedmetadata: function() { audioPlayer.bindings("loadedmetadata"); },
		progress: function() { audioPlayer.bindings("progress"); },
		canplay: function() { audioPlayer.bindings("canplay"); },
		canplaythrough: function() { audioPlayer.bindings("canplaythrough"); }
	});

    //audioPlayer.track.addEventListener("canplaythrough", function () {
        
    //}, false); 

    /*audioPlayer.track.addEventListener("progress", function(e) {
        //console.log("Progress: " + JSON.stringify(e));
        if(audioPlayer.track.buffered.length > 0) {
            console.log("Buffered: " + audioPlayer.track.buffered.end(audioPlayer.track.buffered.length-1) + " / " + audioPlayer.track.duration);
        }  
    }, false);*/

    jq('#audioPlay').click(function(e) {
        e.preventDefault();

        if(audioPlayer.audioPlayerReset) {
            // player is reset, ready to play - 
            audioPlayer.playBeep();
            
            beepInterval = setInterval(function() {
                audioPlayer.playBeep();
                audioPlayer.track.pause();
            }, 2000);

            var timesThrough = 0;

            bufferInterval = setInterval(function() {
                timesThrough++;
                //console.log("Buffer hit: " + timesThrough);

                var buffered = audioPlayer.buffered;
                var bufferedPercent = audioPlayer.bufferedPercent;

                //console.log("State: " + audioPlayer.state);
                
                jq('#buffer-bar').css({'width' : (bufferedPercent) + "%"});

                if(bufferedPercent >= audioPlayer.bufferThreshold || (timesThrough > audioPlayer.localCheck && bufferedPercent < 1)) {
                    clearInterval(beepInterval);
                    clearInterval(bufferInterval);

                    audioPlayer.state == "Loaded";
                    audioPlayer.track.currentTime = 0;
                    audioPlayer.track.volume = 1;
                    audioPlayer.audioPlayerReset = false;
                    audioPlayer.play();
                    jq('#audioPlay').html('<i class="fa fa-pause"></i>');
                    jq(this).find(jq(".fa")).removeClass('fa-play-circle').addClass('fa-pause');
                    jq('#track-status').removeClass("fade-in-and-out");
                    
                    //console.log("Finished buffering!");
                    //console.log("Playing audio");
                    
                } else {
                    if(audioPlayer.buffered > 0) {
                        console.log("Buffering: " + buffered + " (" + bufferedPercent + "%) / " + audioPlayer.track.duration);
                        audioPlayer.track.volume = 0;
                        //audioPlayer.track.currentTime += 30;
                        audioPlayer.track.currentTime = audioPlayer.buffered + audioPlayer.bufferIncrement;
                        //console.log("Seeking: " + audioPlayer.track.currentTime + " (times through: " + timesThrough + ")");
                    }
                    
                    jq('#track-status').html("Loading: " + bufferedPercent + "%");
                } 

            }, audioPlayer.bufferIntervalTime);

            audioPlayer.track.play();
            audioPlayer.track.pause(); // kludge for android devices

            audioPlayer.audioPlayerReset = false;
            audioPlayer.setLoading();
            
            jq('div.animated-text').find("p").each(function() {
                jq(this).removeClass("text-animated-one").removeClass("text-animated-two").css("opacity", 0);
            });

            jq('#track-status').html("Loading...").addClass("fade-in-and-out");
            jq('#audioPlay').html('<i class="fa fa-circle-o-notch fa-spin"></i>');

        } else {
            
            // track is loaded, etc. 

            if(audioPlayer.audioIsPlaying) {
                // pause
                audioPlayer.pause();
                jq(this).find(jq(".fa")).removeClass('fa-pause').addClass('fa-play-circle');
                jq('#track-status').addClass("fade-in-and-out");
            } else {
                // play
                clearInterval(beepInterval);

                audioPlayer.play();
                
                jq(this).find(jq(".fa")).removeClass('fa-play-circle').addClass('fa-pause');
                jq('#track-status').removeClass("fade-in-and-out");
            } 
        }
    });

	// bind buttons for player - should be in a "view" class!

    jq('#track-progress').click(function(e) {
        e.preventDefault();
        if(audioPlayer.isLoading()) {
            return false;
        }
        var percentage = (Math.round(((e.pageX - (jq(this).offset().left)) / (jq(this).width())) * 100 * 100) / 100).toFixed();
        audioPlayer.goto(percentage);
    });

	jq('#audioButton0').click(function(e) {
        // stop
        e.preventDefault();

        if(audioPlayer.isLoading()) {
        	clearInterval(beepInterval);
            clearInterval(bufferInterval);
        }

		audioPlayer.stop();
        audioPlayer.reset();
        jq('#track-status').removeClass("fade-in-and-out");
		jq('#audioPlay').html('<i class="fa fa-play-circle"></i>');
        jq('#buffer-bar').css({'width' : "0%"});
        jq('#progress-bar').css({'width' : "0%"});
    });

    jq('#audioButton1').click(function(e) {
        // scrub backward
        e.preventDefault();
        audioPlayer.scrub(-10);
    });

    jq('#audioButton2').click(function(e) {
        // scrub forward
        e.preventDefault();
        audioPlayer.scrub(10);
    });

    // close button + alias events!

    jq('#close').click(function(e) {
        e.preventDefault();
        clearInterval(beepInterval);
        clearInterval(bufferInterval);
        audioPlayer.stop();
        audioPlayer.reset();

        var closeButton = jq(this);
        closeButton.addClass('pressDown');
        
        setTimeout(function() {
            closeButton.removeClass('pressDown');
            jq('#audioPlay').html('<i class="fa fa-play-circle"></i>');
            jq('#listen-now').fadeOut(500, function() {
                jq('div.main-section').fadeIn(300);
            });
        }, 10);
    });

    jq('#audioButton3').click(function(){
        // close now playing area
        jq('#close').click();
    });

    jq(document).keyup(function(e) {
        if(e.which == 27) {
        	// close now playing area
            jq('#close').click();
        }
    });

    // load different track
    // later, this will have a call to audioPlayer.loadTrack(trackSrc);

    jq('.loadAudio').click(function() {
        var thisItem = jq(this);
        thisItem.addClass('pressDown');

        // adjust listen now area
        var bg = jq(this).data("bg");
        var cover = jq(this).data("cover");
        var icon = jq(this).data("icon");
        var cat = jq(this).data("cat");
        var title = jq(this).data("title");
        var rgb = jq(this).data("rgb");
        var stream = jq(this).data("stream");
        var duration = jq(this).data("duration");
        //console.log("Stream: " + stream);

        jq('#listen-now-bg').css("background-image", "url('" + bg + "')");
        
        jq('#audio-player').css("background-color", function() {
            if(jq.browser.msie && parseInt(jq.browser.version, 10) === 8) 
                return rgb;
            else 
                return "rgba(255, 255, 255, 0.2)";
        });

        jq('#cover-holder').css("background-image", "url('" + cover + "')");
        jq('#track-cover').attr('src', cover);
        jq('#track-icon').attr('src', icon);
        
        jq('#track-info').find("em").html(cat);
        jq('#track-info').find("strong").html(title);
        
        audioPlayer.loadTrack(stream);

        //jq('#mainPlayer').attr('src', stream);

        audioPlayer.updateDuration(duration); // ???

        setTimeout(function() {
            thisItem.removeClass('pressDown');
            jq('#listen-now').ready(function() {
                jq('div.main-section').fadeOut(300, function() {
                    jq('#listen-now').fadeIn(100, function() {
                    });
                });                
            });
        }, 500);
        
    });

    var filtering = false;
    var filteringType = "";

    jq('.filter-link').click(function() {
        var rel = jq(this).attr("rel");
        jq('section.streams-holder article').css('opacity', '1');

        if(rel == "none") {
            filtering = none;
            filteringType = "";
            return;
        }

        if(filtering) {
            if(filteringType == rel) {
                filtering = false;
                filteringType = "";
            } else {
                jq('section.streams-holder article').not('.' + rel + ', .add-new').css('opacity', '0.5');
                filteringType = rel;
            }
        } else {
            jq('section.streams-holder article').not('.' + rel + ', .add-new').css('opacity', '0.5');
            filtering = true;
            filteringType = rel;
        }
        
        
    });

    jq('article.shop-now').click(function() {
        window.open("https://quietself.com/shop", "_blank");
    });

    // var myTracks = jq('.loadAudio').toArray();
    // myTracks[1].click(); 

    jq(window).load(function() {
        jq('#preloader').fadeOut('fast', function() {
            jq('#easy-streamer-holder').css('opacity', '1');
        });        
    });

});

//console.log("jQuery loaded - streaming audio loaded");