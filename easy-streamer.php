<?php

/*
 * Plugin Name:       Easy Streamer
 * Plugin URI:        http://www.anguaji.com/io/wp/plugins/easy-streaming/
 * Description:       Adds bespoke streaming page to stream Easy Digital Downloads!
 * Version:           0.5
 * Author:            Anguaji Development
 * Author URI:        http://www.anguaji.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       easy-streamer
 * Domain Path:       /languages
 */

defined('ABSPATH') or die('No script kiddies please!');

global $wp_version;

if(!version_compare($wp_version, "4.0", ">=")) {
	die("Whoops! You need at least version 4.0 of Wordpress to use the Easy Streamer plugin :(");
}

/* Activation and deactivation hooks */

function easy_streamer_activate() {
	error_log("Easy Streamer is now activated");
}

register_activation_hook(__FILE__, "easy_streamer_activate");

function easy_streamer_deactivate() {
	error_log("Easy Streamer is now deactivated");
}

register_deactivation_hook(__FILE__, "easy_streamer_deactivate");

/* Main shortcode functions */

function easy_streamer_call() {
	
/*
 * This template is used to display the download history of the current user.
 */
$purchases = edd_get_users_purchases( get_current_user_id(), 20, true, 'any' );
if ( $purchases ) :
	//do_action( 'edd_before_download_history' ); ?>
	<table id="edd_user_history">
		<thead>
			<tr class="edd_download_history_row">
				<?php do_action( 'edd_download_history_header_start' ); ?>
				<th class = "edd_download_download_name">ID</th>
				<th class = "edd_download_download_name">Image</th>
				<th class="edd_download_download_name"><?php _e( 'Meditation Name', 'easy-digital-downloads' ); ?></th>
				<?php if ( ! edd_no_redownload() ) : ?>
					<th class="edd_download_download_files"><?php _e( 'Click Play To Listen On Your Device, Right Now!', 'easy-digital-downloads' ); ?></th>
				<?php endif; //End if no redownload?>
				<?php do_action( 'edd_download_history_header_end' ); ?>
			</tr>
		</thead>
		<?php foreach ( $purchases as $payment ) :
			$downloads      = edd_get_payment_meta_cart_details( $payment->ID, true );
			$purchase_data  = edd_get_payment_meta( $payment->ID );
			$email          = edd_get_payment_user_email( $payment->ID );

			if ( $downloads ) :
				foreach ( $downloads as $download ) :

					// Skip over Bundles. Products included with a bundle will be displayed individually
					if ( edd_is_bundled_product( $download['id'] ) )
						continue; ?>

					<tr class="edd_download_history_row">
						<?php
						$price_id 		= edd_get_cart_item_price_id( $download );
						$download_files = edd_get_download_files( $download['id'], $price_id );
						$name           = get_the_title( $download['id'] );
						$post_id 		= $download['id'];
						$category 		= get_the_category($post_id);
						
						$stream_assets = get_mapped_item($post_id);
						if($stream_assets !== false) {
							?><pre><?php print_r($stream_assets); ?></pre><?php
						}

						// Retrieve and append the price option name
						if ( ! empty( $price_id ) ) {
							$name .= ' - ' . edd_get_price_option_name( $download['id'], $price_id, $payment->ID );
						}

						do_action( 'edd_download_history_row_start', $payment->ID, $download['id'] );
						?>
						<td><?php print $download['id'] ?></td>
						<td class = "edd_download_download_name"><div class="edd_download_image">
		<a href="Javascript:;" title="<?php print $name; ?>">
			<?php echo get_the_post_thumbnail($post_id, 'thumbnail'); ?>
		</a>
	</div></td>

						<td class="edd_download_download_name"><?php echo esc_html( $name ); ?></td>

						<?php if ( ! edd_no_redownload() ) : ?>
							<td class="edd_download_download_files">
								<?php

								if ( edd_is_payment_complete( $payment->ID ) ) :

									if ( $download_files ) :

										foreach ( $download_files as $filekey => $file ) :

											$download_url = edd_get_download_file_url( $purchase_data['key'], $email, $filekey, $download['id'], $price_id );
											
											?>

											<div class="edd_download_file">
												<a href="<?php echo esc_url( $download_url ); ?>" class="edd_download_file_link">
													<?php echo isset( $file['name'] ) ? esc_html( $file['name'] ) : esc_html( $name ); ?><br />
												</a>
												<audio controls>

													<source src="<?php print $download_url ?>" type="audio/mpeg">

													Error: Your browser does not support the audio element.

												</audio>
												
											</div>

											<?php do_action( 'edd_download_history_files', $filekey, $file, $id, $payment->ID, $purchase_data );
										endforeach;

									else :
										_e( 'No downloadable files found.', 'easy-digital-downloads' );
									endif; // End if payment complete

								else : ?>
									<span class="edd_download_payment_status">
										<?php printf( __( 'Payment status is %s', 'easy-digital-downloads' ), edd_get_payment_status( $payment, true) ); ?>
									</span>
									<?php
								endif; // End if $download_files
								?>
							</td>
						<?php endif; // End if ! edd_no_redownload()

						do_action( 'edd_download_history_row_end', $payment->ID, $download['id'] );
						?>
					</tr>
					<?php
				endforeach; // End foreach $downloads
			endif; // End if $downloads
		endforeach;
		?>
	</table>
	<div id="edd_download_history_pagination" class="edd_pagination navigation">
		<?php
		$big = 999999;
		echo paginate_links( array(
			'base'    => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
			'format'  => '?paged=%#%',
			'current' => max( 1, get_query_var( 'paged' ) ),
			'total'   => ceil( edd_count_purchases_of_customer() / 20 ) // 20 items per page
		) );
		?>
	</div>
	<?php do_action( 'edd_after_download_history' ); ?>
<?php else : ?>

	<?php 
		if(!is_user_logged_in()) {
			?>
				<p class="edd-no-downloads">Hi there! Please <a href = "<?php print get_bloginfo('wpurl') ?>/customer-login/">log in</a> to view your meditations.</p>
			<?php
		} else {
			?>
				<p class="edd-no-downloads"><?php _e( 'You have not purchased any downloads', 'easy-digital-downloads' ); ?></p>
			<?php
		}
	?>
<?php endif; ?>


	<?php 
}

add_shortcode('easy_streamer_page', 'easy_streamer_call');

function get_streams() {

	$purchases = edd_get_users_purchases(get_current_user_id(), 20, true, 'any');

	if (!$purchases)
		return false;

	$streams = array();

	foreach ( $purchases as $payment ) :
		$downloads      = edd_get_payment_meta_cart_details( $payment->ID, true );
		$purchase_data  = edd_get_payment_meta( $payment->ID );
		$email          = edd_get_payment_user_email( $payment->ID );

		if ($downloads) :
			foreach ($downloads as $download) :

				// Skip over Bundles. Products included with a bundle will be displayed individually
				if (edd_is_bundled_product($download['id']))
					continue; 

				$price_id 		= edd_get_cart_item_price_id( $download );
				$download_files = edd_get_download_files( $download['id'], $price_id );
				$name           = get_the_title( $download['id'] );
				$post_id 		= $download['id'];
				$category 		= get_the_category($post_id);
				
				if (edd_is_payment_complete($payment->ID)) {
					foreach ($download_files as $filekey => $file) {
						
						//print "File: <pre>".print_r($file)."</pre><br />";
						$download_url = edd_get_download_file_url($purchase_data['key'], $email, $filekey, $download['id'], $price_id);
						//print "Download_url: ".$download_url."<br />";

						$stream_assets = get_mapped_item($post_id);
						$stream_assets['original_stream'] = $stream_assets['stream_link'];
						$stream_assets['stream_link'] = $download_url;

						// Retrieve and append the price option name
						if (!empty($price_id)) {
							$stream_assets['long_title'] = $stream_assets['title'] . ' - '.edd_get_price_option_name($download['id'], $price_id, $payment->ID);
						}

						if($stream_assets !== false) {
							$streams[] = $stream_assets;
						}
					}					
				}

			endforeach; // End foreach $downloads
		endif; // End if $downloads
	endforeach;

	if(count($streams) > 0)
		return $streams;
	else 
		return false;

}

$ES_STATIC_ID;

function streaming_page_full() {

	global $ES_STATIC_ID;
	
	$ES_STATIC_ID++;
	$streamcount = 4;
	$x = 0;
	
	$es_title = get_option('es_title') !== false && get_option("es_title") != "" ? esc_attr(get_option('es_title')) : "Your Meditation Library";
	$es_description = get_option('es_description') !== false && get_option("es_description") != "" ? str_replace("\n", "<br />", get_option('es_description')) : "The audio tracks you've bought are available here for you to listen to any time.<br />If you're listening on a mobile device, for best results use headphones or earbuds.";
	$es_bg_image = get_option('es_bg_image') !== false && get_option("es_bg_image") != "" ? esc_attr(get_option('es_bg_image')) : plugin_dir_url(__FILE__)."template-include/canyon-view.jpg";
	$es_icon_image = get_option('es_icon_image') !== false && get_option("es_icon_image") != "" ? esc_attr(get_option('es_icon_image')) : plugin_dir_url(__FILE__)."template-include/logo-headphones.png";

	$es_welcome_title = get_option('es_welcome_title') !== false && get_option("es_welcome_title") != "" ? esc_attr(get_option('es_welcome_title')) : "Welcome!";
	$es_welcome_cat = get_option('es_welcome_cat') !== false && get_option("es_welcome_cat") != "" ? esc_attr(get_option('es_welcome_cat')) : "Welcome To QuietSelf.com";
	$es_welcome_stream = get_option('es_welcome_stream') !== false && get_option("es_welcome_stream") != "" ? esc_attr(get_option('es_welcome_stream')) : plugin_dir_url(__FILE__)."template-include/Welcome.mp3";
	$es_welcome_duration = get_option('es_welcome_duration') !== false && get_option("es_welcome_duration") != "" ? esc_attr(get_option('es_welcome_duration')) : "5:00";
	$es_welcome_bg_image = get_option('es_welcome_bg_image') !== false && get_option("es_welcome_bg_image") != "" ? esc_attr(get_option('es_welcome_bg_image')) : plugin_dir_url(__FILE__)."template-include/Welcome-bg.jpg";
	$es_welcome_cover_image = get_option('es_welcome_cover_image') !== false && get_option("es_welcome_cover_image") != "" ? esc_attr(get_option('es_welcome_cover_image')) : plugin_dir_url(__FILE__)."template-include/Welcome-cover.jpg";

	$es_buffer_threshold = get_option('es_buffer_threshold') !== false && get_option("es_buffer_threshold") != "" ? esc_attr(get_option('es_buffer_threshold')) : 50;
	$es_buffer_increment = get_option('es_buffer_increment') !== false && get_option("es_buffer_increment") != "" ? esc_attr(get_option('es_buffer_increment')) : 30;
	$es_buffer_interval = get_option('es_buffer_interval') !== false && get_option("es_buffer_interval") != "" ? esc_attr(get_option('es_buffer_interval')) : 300;
	$es_local_check = get_option('es_local_check') !== false && get_option("es_local_check") != "" ? esc_attr(get_option('es_local_check')) : 30;
	
	if(!is_user_logged_in()) {
		?>
			<div class = "main-section" style = "background-image: url('<?php print $es_bg_image ?>'); background-size: cover; background-attachment: fixed; background-position: center;">

			<section class = "bg-head">
				<div>
					<p><img width = "100" src = "<?php print $es_icon_image ?>" /></p><h1 class = "page"><?php print $es_title ?></h1>					
				</div>
			</section>

			<div class = "main-content">

				<section class = "streams-holder">

					<p class="edd-no-downloads">Welcome!<br /><br />Please <a href = "<?php print get_bloginfo('wpurl') ?>/customer-login/">log in</a> to view your meditations.<br /><br />If you're new to QuietSelf.com, get started with our free <a href = "https://quietself.com/body-relaxation/">10 minute Body Relaxation meditation.</p>

				</section>

			</div>

		</div>
		<?php

		return; 
	} 

	// user is logged in, continue: 
	$streams = get_streams();

	?>
	<div id = "preloader" style = "position: fixed; left: 0; top: 0; z-index: 999; width: 100%; height: 100%; overflow: visible; background: #000 url('<?php print plugin_dir_url(__FILE__)."template-include/loading-2.gif";  ?>') no-repeat center center;"></div>

	<div id = "easy-streamer-holder" class = "easy-streamer-holder" style = "opacity: 0;">

		<div id = "listen-now" style = "display: none;">
			
			<div class = "listen-now-bg" id = "listen-now-bg"></div>
			
			<section class = "audio-player" id = "audio-player">
				<header>
					
					<a href = "Javascript:;" id = "close" class = "noSelect"><i class = "fa fa-times-circle-o noSelect"></i></a>

					<div class = "cover-holder" id = "cover-holder">
						<img id = "track-cover" src = "<?php print plugin_dir_url(__FILE__) ?>template-include/Welcome-cover.jpg" alt = "Track Image" />
						<img id = "track-icon" src = "<?php print plugin_dir_url(__FILE__) ?>template-include/introspective.png" />
					</div>

					<section class = "track-info" id = "track-info">
						<em>Introspective Music</em>
						<strong id = "track-title">To A Further Shore</strong>
						<p id = "track-status"></p>
					</section>

					<br style = "clear: both" />

				</header>
				
				<section id = "player-controls" class = "player-controls">

					<a id = "track-progress" class = "noselect">
						<div class = "track-progress" id = "progress-bar"></div>
						<div class = "track-progress" id = "buffer-bar"></div>
					</a>
					
					<div class = "controls-holder">

						<audio id = "mainPlayer" 
						controls = "controls" 
						style = "display: none; height: 0px; width: 0px;" 
						data-threshold = "<?php print $es_buffer_threshold ?>" 
						data-increment = "<?php print $es_buffer_increment ?>" 
						data-interval = "<?php print $es_buffer_interval ?>"
						data-local = "<?php print $es_local_check ?>">
							<source preload = "auto" src="<?php print plugin_dir_url(__FILE__) ?>template-include/high-bowl-8sec.mp3" type="audio/mpeg">
							Error: Your browser does not support the audio element.
						</audio>
						
						<audio id = "beep" style = "display: none; height: 0px; width: 0px;">
							<source preload = "auto" src="<?php print plugin_dir_url(__FILE__) ?>template-include/high-bowl-8sec.mp3" type="audio/mpeg">
								Error: Audio not supported
						</audio>

						<ul class = "custom-controls">
							<li class = "small"><a href = "Javascript:;" id = "audioButton0" class = "audioButton smaller noSelect disabled"><i class = "fa fa-stop-circle noSelect"></i></a></li>
							<!--<li class = "small"><a href = "Javascript:;" id = "audioButton1" class = "audioButton smaller noSelect disabled hidden"><i class = "fa fa-backward noSelect"></i></a></li>-->
							<li class = "large"><a href = "Javascript:;" id = "audioPlay" class = "audioButton noSelect"><i class = "fa fa-play-circle noSelect"></i></a></li>
							<!--<li class = "small"><a href = "Javascript:;" id = "audioButton2" class = "audioButton smaller noSelect  disabled hidden"><i class = "fa fa-forward noSelect"></i></a></li>-->
							<li class = "small"><a href = "Javascript:;" id = "audioButton3" class = "audioButton smaller noSelect disabled"><i class = "fa fa-times-circle-o noSelect"></i></a></li>
							<br style = "clear: both" />
						</ul>

					</div>

				</section>
			
			</section>

			<section id = "player-icons" style = "display: none;">

				<div class = "animated-text">
					<p class = "instructions text-animated-one">
						For the best experience, use headphones. 
					</p>

					<p class = "instructions text-animated-two">
						Hit play to start. 
					</p>
				</div>

			</section>

		</div>

		<div class = "main-section" style = "background-image: url('<?php print $es_bg_image ?>'); background-size: cover; background-attachment: fixed; background-position: center;">

			<section class = "bg-head">
				<div>
					<p><img width = "100" src = "<?php print $es_icon_image ?>" /></p><h1 class = "page"><?php print $es_title ?></h1>
					<p class = "bg"><?php print $es_description ?></p>
					
				</div>
			</section>

			<div class = "main-content">

				<section class = "filters">
					<ul>
						<li>Filter By:</li>
						<li><a href = "Javascript:;" class = "filter-link active" rel = "none">* None</a></li>
						<li><a href = "Javascript:;" class = "filter-link" rel = "enhanced">Enhanced Meditation</a></li>
						<li><a href = "Javascript:;" class = "filter-link" rel = "music">Music Meditation</a></li>
						<li><a href = "Javascript:;" class = "filter-link" rel = "introspective">Introspective Music</a></li>
						<br style = "clear: both;" />
					</ul>
				</section>

				<section class = "streams-holder">

				<article class = "loadAudio noSelect introspective" 
					style = "background-image: url('<?php print $es_welcome_cover_image ?>');" 
					data-bg = "<?php print $es_welcome_bg_image ?>" 
					data-title = "<?php print $es_welcome_title ?>" 
					data-cat = "<?php print $es_welcome_cat ?>" 
					data-icon = "<?php print plugin_dir_url(__FILE__) ?>template-include/logo-headphones.png" 
					data-cover = "<?php print $es_welcome_cover_image ?>" 
					data-rgb = "rgb(16,16,28)" 
					data-stream = "<?php print $es_welcome_stream ?>"
					data-duration = "<?php print $es_welcome_duration ?>">
					<span class = "icon"><img width = "20" src = "<?php print plugin_dir_url(__FILE__) ?>template-include/logo-headphones.png" /></span>
					<span class = "duration"><?php print $es_welcome_duration ?></span>
					<p>
						<a href = "Javascript:;"><i class = "fa fa-play-circle"></i> <?php print $es_welcome_title ?></a>
						<span class = "desc"><i class = "fa fa-info-circle"></i> Helpful Hints</span>
					</p>

				</article>

				<?php

					$icons['e'] = plugin_dir_url(__FILE__)."template-include/enhanced.png";
					$icons['i'] = plugin_dir_url(__FILE__)."template-include/introspective.png";
					$icons['m'] = plugin_dir_url(__FILE__)."template-include/meditation.png";
					
					$cats['e'] = "Enhanced Meditation";
					$cats['i'] = "Introspective Music";
					$cats['m'] = "Music Meditation";

					$rels['e'] = "enhanced";
					$rels['i'] = "introspective";
					$rels['m'] = "music";

					$bg_array = array(plugin_dir_url(__FILE__)."template-include/Welcome-bg.jpg");

					if(is_array($streams)) {
					
						foreach($streams as $stream) {
							$cover = $stream['cover'];

							if($stream['title'] == "") {
								continue;
							}

							$title = $stream['title'];
							$bg = $stream['bg'];
							$bg_array[] = $bg;
							$description = $stream['description'];
							$cat = $stream['category'];
							$duration = $stream['duration'];
							
							if($stream['original_stream'] != "#" && $stream['original_stream'] != "") {
								$stream_url = $stream['original_stream'];
							} else {
								$stream_url = $stream['stream_link'];
							}

							if(is_file($cover)) {
								$im = imagecreatefromjpeg($cover);
								$rgb = imagecolorat($im, 10, 15);
								$r = ($rgb >> 16) & 0xFF;
								$g = ($rgb >> 8) & 0xFF;
								$b = $rgb & 0xFF;
							} else {
								$r = 150;
								$g = 150;
								$b = 150;
							}

							?><article class = "loadAudio noSelect <?php print $rels[$cat] ?>" 
							style = "background-image: url('<?php print $cover ?>');" 
							data-bg = "<?php print $bg ?>" 
							data-title = "<?php print $title ?>" 
							data-cat = "<?php print $cats[$cat]; ?>" 
							data-icon = "<?php print $icons[$cat]; ?>" 
							data-cover = "<?php print $cover ?>" 
							data-rgb = "rgb(<?php print $r.",".$g.",".$b.")"; ?>" 
							data-stream = "<?php print $stream_url ?>"
							data-duration = "<?php print $stream['duration'] ?>">
							<span class = "icon"><img src = "<?php print $icons[$cat]; ?>" /></span>
							<span class = "duration"><?php print $duration ?></span>

							<p>
								<a href = "Javascript:;"><i class = "fa fa-play-circle"></i> <?php print $title ?></a>
								<span class = "desc"><i class = "fa fa-info-circle"></i> <?php print $description ?></span>
							</p>

							</article><?php
						}

					}

					?>

					<article class = "shop-now" style = "background-image: url('<?php print plugin_dir_url(__FILE__) ?>template-include/Add-Icon.jpg');" data-bg = "<?php print $bg ?>"></article>

					<br style = "clear: both;" />

					</section>

					<section class = "streams-holder">

						<hr class = "stream-header" />

					<h2 class = "stream-header">Complimentary Content</h2>

					
						
						<?php

					$audio_posts = get_option("es_audio_posts");

					if(is_array($audio_posts) && count($audio_posts) > 0) {

						$audio_posts = array_reverse($audio_posts);
					
						foreach($audio_posts as $stream) {
							if($stream['visible'] == "false") {
								continue;
							} 

							$cover = $stream['cover'];
							$title = $stream['title'];
							$bg = $stream['bg'];
							$bg_array[] = $bg;
							//$description = $stream['description'];
							$cat = $stream['category'];
							$duration = $stream['duration'];

							if(is_file($cover)) {
								$im = imagecreatefromjpeg($cover);
								$rgb = imagecolorat($im, 10, 15);
								$r = ($rgb >> 16) & 0xFF;
								$g = ($rgb >> 8) & 0xFF;
								$b = $rgb & 0xFF;
							} else {
								$r = 150;
								$g = 150;
								$b = 150;
							}

							?><article class = "loadAudio noSelect <?php print $rels[$cat] ?>" 
							style = "background-image: url('<?php print $cover ?>');" 
							data-bg = "<?php print $bg ?>" 
							data-title = "<?php print $title ?>" 
							data-cat = "<?php print $stream[$cat]; ?>" 
							data-icon = "<?php print plugin_dir_url(__FILE__) ?>template-include/logo-headphones.png" 
							data-cover = "<?php print $cover ?>" 
							data-rgb = "rgb(<?php print $r.",".$g.",".$b.")"; ?>" 
							data-stream = "<?php print $stream['stream'] ?>"
							data-duration = "<?php print $stream['duration'] ?>">
							<span class = "icon"><img src = "<?php print plugin_dir_url(__FILE__) ?>template-include/logo-headphones.png" /></span>
							<span class = "duration"><?php print $duration ?></span>
							
							<p>
								<a href = "Javascript:;"><i class = "fa fa-play-circle"></i> <?php print $title ?></a><br />
							</p>

							</article><?php
						}

					}

					?>

					<br style = "clear: both;" />
				</section>

			</div>

			<div class="hidden">

				<i class = "fa fa-pause"></i>
				<i class = "fa fa-pause-circle"></i>
				<i class = "fa fa-pause-circle-o"></i>
				
				<?php
					$bg_string = implode('","', $bg_array);
					$bg_string = '"'.$bg_string.'"';
				?>

				<script type="text/javascript">
					<!--//--><![CDATA[//><!--
						var images = new Array()
						function preload() {
							for (i = 0; i < preload.arguments.length; i++) {
								images[i] = new Image()
								images[i].src = preload.arguments[i]
							}

							//alert("Loaded");
						}

						//alert('array string: <?php print $bg_string ?>');
						preload(<?php print $bg_string ?>);
					//--><!]]>
				</script>
			</div>

		</div><!-- main section -->

	</div><!-- easy-streamer-holder -->

	<?php
}

add_shortcode('streaming_page_full', 'streaming_page_full');

function streaming_downloads_page() {

	global $ES_STATIC_ID;
	
	$ES_STATIC_ID++;
	$streamcount = 4;
	$x = 0;
	
	$es_title = get_option('es_title_downloads') !== false && get_option("es_title_downloads") != "" ? esc_attr(get_option('es_title_downloads')) : "Your Meditation Downloads";
	$es_description = get_option('es_description_downloads') !== false && get_option("es_description_downloads") != "" ? str_replace("\n", "<br />", get_option('es_description_downloads')) : "The audio tracks you've bought are available for you to download to your devices.";
	$es_bg_image = get_option('es_bg_image_downloads') !== false && get_option("es_bg_image_downloads") != "" ? esc_attr(get_option('es_bg_image_downloads')) : plugin_dir_url(__FILE__)."template-include/canyon-view.jpg";
	$es_icon_image = get_option('es_icon_image_downloads') !== false && get_option("es_icon_image_downloads") != "" ? esc_attr(get_option('es_icon_image_downloads')) : plugin_dir_url(__FILE__)."template-include/logo-headphones.png";
		
	if(!is_user_logged_in()) {
		?>
			<div class = "main-section" style = "background-image: url('<?php print $es_bg_image ?>'); background-size: cover; background-attachment: fixed; background-position: center;">

			<section class = "bg-head">
				<div>
					<p><img src = "<?php print $es_icon_image ?>" /></p><h1 class = "page"><?php print $es_title ?></h1>				
				</div>
			</section>

			<div class = "main-content">

				<section class = "streams-holder">

					<p class="edd-no-downloads">Welcome! Please <a href = "<?php print get_bloginfo('wpurl') ?>/customer-login/">log in</a> to view your downloads.</p>

				</section>

			</div>

		</div>
		<?php

		return; 
	} 

	?>

	<?php
	
	$streams = get_streams();

	?>

	<div class = "easy-streamer-holder" style = "opacity: 1;">

		<div class = "main-section" style = "background-image: url('<?php print $es_bg_image ?>'); background-size: cover; background-attachment: fixed; background-position: center;">

			<section class = "bg-head">
				<div>
					<p><img src = "<?php print $es_icon_image ?>" /></p><h1 class = "page"><?php print $es_title ?></h1>
					<p class = "bg"><?php print $es_description ?></p>
					
				</div>
			</section>

			<div class = "main-content">

				<section class = "streams-holder">
				<br /><br />
				<table id="edd_user_history">
					<thead>
						<tr class="edd_download_history_row">
							<th class = "edd_download_download_name">Cover</th>
							<th class = "edd_download_download_name">Name</th>
							<th class = "edd_download_download_name">Download</th>
						</tr>
					</thead>

				<?php

					$icons['e'] = plugin_dir_url(__FILE__)."template-include/enhanced.png";
					$icons['i'] = plugin_dir_url(__FILE__)."template-include/introspective.png";
					$icons['m'] = plugin_dir_url(__FILE__)."template-include/meditation.png";
					
					$cats['e'] = "Enhanced Meditation";
					$cats['i'] = "Introspective Music";
					$cats['m'] = "Music Meditation";

					$rels['e'] = "enhanced";
					$rels['i'] = "introspective";
					$rels['m'] = "music";

					if(is_array($streams)) {

						foreach($streams as $stream) {
							$cover = $stream['cover'];
							$title = $stream['title'];
							$bg = $stream['bg'];
							$description = $stream['description'];
							$cat = $stream['category'];

							?>
							<tr class="edd_download_history_row">
								<td class = "edd_download_download_name" valign = "middle" align = "center"><img src = "<?php print $cover ?>" style = "vertical-align: middle; text-align: center;" /></td>
								<td class = "edd_download_download_name" valign = "middle"><strong><?php print $title ?></strong><br /><?php print $description ?></td>
								<td class = "edd_download_download_name" valign = "middle">
									<a href = "<?php print $stream['stream_link'] ?>" download><i class = "fa fa-download"></i> Download MP3 File</a>
									<br />
									<p class = "small" style = "color: #888; line-height: 1.6em;"><em>On Computer: right click the link and choose "Save Link As..." to download the file.<br />On Mobile: long press the link above and choose "Save" if prompted.</em></p>
								</td>
							</tr>

							<?php
						}

					}

					?>

				</table>

				</section>

			</div>

			<div class="hidden">
				<audio id = "mainPlayer" controls = "controls" style = "display: none; height: 0px; width: 0px;">
					<source src="<?php print plugin_dir_url(__FILE__) ?>template-include/high-bowl-8sec.mp3" type="audio/mpeg">
					Error: Your browser does not support the audio element.
				</audio>
				
				<audio id = "beep" style = "display: none; height: 0px; width: 0px;">
					<source src="<?php print plugin_dir_url(__FILE__) ?>template-include/high-bowl-8sec.mp3" type="audio/mpeg">
					Error: Audio not supported
				</audio>

			</div>

		</div><!-- main section -->

	</div><!-- easy-streamer-holder -->

	<?php
}

add_shortcode('streaming_page_downloads', 'streaming_downloads_page');

function easy_streamer_setup_js() {
	global $ES_STATIC_ID;

	if($ES_STATIC_ID > 0) {

		wp_register_style('font-awesome', plugin_dir_url(__FILE__).'template-include/font-awesome/css/font-awesome.min.css');
		wp_register_style('easy-streamer-css', plugin_dir_url(__FILE__).'template-include/proto-streaming.css');
		wp_register_script('easy-streamer-js', plugin_dir_url(__FILE__).'template-include/proto-streaming-new.js', array('jquery'), '1.0', true);
		
		wp_enqueue_style('font-awesome');
		wp_enqueue_style('easy-streamer-css');
		wp_enqueue_script('easy-streamer-js');
	}
}

add_action('wp_footer', 'easy_streamer_setup_js');

function easy_streamer_page_getter($page_template) {
    if(is_page('streaming') || is_page('library-downloads') || is_page('library')) {
        $page_template = dirname( __FILE__ ) . '/page-stream-page.php';
    }

    return $page_template;
}

add_filter( 'page_template', 'easy_streamer_page_getter' );

/** ADMIN OPTIONS **/
function es_plugin_register_settings() {
	//register our settings
	register_setting('es-settings-group', 'es_title');
	register_setting('es-settings-group', 'es_description');
	register_setting('es-settings-group', 'es_bg_image');
	register_setting('es-settings-group', 'es_icon_image');

	register_setting('es-settings-group', 'es_title_downloads');
	register_setting('es-settings-group', 'es_description_downloads');
	register_setting('es-settings-group', 'es_bg_image_downloads');
	register_setting('es-settings-group', 'es_icon_image_downloads');

	register_setting('es-settings-group', 'es_welcome_title');
	register_setting('es-settings-group', 'es_welcome_cat');
	register_setting('es-settings-group', 'es_welcome_bg_image');
	register_setting('es-settings-group', 'es_welcome_cover_image');
	register_setting('es-settings-group', 'es_welcome_stream');
	register_setting('es-settings-group', 'es_welcome_duration');

	register_setting('es-settings-group', 'es_mappings');
	register_setting('es-settings-group', 'es_audio_posts');

	register_setting('es-settings-group', 'es_buffer_threshold');
	register_setting('es-settings-group', 'es_buffer_increment');
	register_setting('es-settings-group', 'es_buffer_interval');
	register_setting('es-settings-group', 'es_local_check');

}

// UPLOAD ENGINE
function load_es_wp_media_files() {
    wp_enqueue_media();
}
add_action('admin_enqueue_scripts', 'load_es_wp_media_files');

function get_mapped_item($wp_id) {
	$mappings = get_option("es_mappings");

	if(!$mappings || count($mappings) < 1)
		return false;

	foreach($mappings as $item) {
		if($item['wp_id'] == $wp_id)
			return $item;
	}

	return false;
}

function es_plugin_options() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	
	?>
	<div class="wrap">
	<h2>Easy Streamer Plugin Menu</h2>

	<form method="post" action="options.php">
	    <?php settings_fields('es-settings-group'); ?>
	    <?php do_settings_sections('es-settings-group'); ?>

	    <h3>Streaming Page</h3>
	    <table class="form-table">
	        
	        <tr valign="top">
	        <th scope="row">Streaming Page Title</th>
	        <td><input class = "regular-text" type="text" name="es_title" value="<?php echo esc_attr( get_option('es_title') ); ?>" /></td>
	        </tr>

	        <tr valign="top">
	        <th scope="row">Streaming Page Description</th>
	        <td><textarea class = "regular-textarea" name="es_description" style = "width: 350px; height: 100px;"><?php echo esc_textarea( get_option('es_description') ); ?></textarea></td>
	        </tr>
	        
	       	<tr valign = "top">
			    <th scope="row">Streaming Page Header Image</th>
			    <td>
			    	<input type="text" name="es_bg_image" value = "<?php echo esc_attr(get_option('es_bg_image')); ?>" id="es_bg_image" class="regular-text">
			    	<input rel = "es_bg_image" type="button" name="upload-btn-1" id="upload-btn-1" class="button-secondary image-upload-button" value="Upload Image">
			    	<input rel = "es_bg_image" type="button" name="reset-btn-1" id="reset-btn-1" class="button-secondary image-reset-button" value="Reset Image">
			    </td>
			</tr>

			<tr valign = "top">
			    <th scope="row">Streaming Page Icon Image</th>
			    <td>
			    	<input type="text" name="es_icon_image" value = "<?php echo esc_attr(get_option('es_icon_image')); ?>" id="es_icon_image" class="regular-text">
			    	<input rel = "es_icon_image" type="button" name="upload-btn-1" id="upload-btn-1" class="button-secondary image-upload-button" value="Upload Image">
			    	<input rel = "es_icon_image" type="button" name="reset-btn-1" id="reset-btn-1" class="button-secondary image-reset-button" value="Reset Image">
			    </td>
			</tr>

			<tr valign="top">
	        <th scope="row">Buffer Threshold (%)</th>
	        <td><input class = "regular-text" type="text" name="es_buffer_threshold" value="<?php echo esc_attr( get_option('es_buffer_threshold') ); ?>" /></td>
	        </tr>

	        <tr valign="top">
	        <th scope="row">Buffer Increment (ms)</th>
	        <td><input class = "regular-text" type="text" name="es_buffer_increment" value="<?php echo esc_attr( get_option('es_buffer_increment') ); ?>" /></td>
	        </tr>

	        <tr valign="top">
	        <th scope="row">Buffer Interval (ms)</th>
	        <td><input class = "regular-text" type="text" name="es_buffer_interval" value="<?php echo esc_attr( get_option('es_buffer_interval') ); ?>" /></td>
	        </tr>

	        <tr valign="top">
	        <th scope="row">Local file failsafe loops</th>
	        <td><input class = "regular-text" type="text" name="es_local_check" value="<?php echo esc_attr( get_option('es_local_check') ); ?>" /></td>
	        </tr>

	    </table>

	    <h3>Downloads Page</h3>
	    <table class="form-table">
	        
	        <tr valign="top">
	        <th scope="row">Downloads Page Title</th>
	        <td><input class = "regular-text" type="text" name="es_title_downloads" value="<?php echo esc_attr( get_option('es_title_downloads') ); ?>" /></td>
	        </tr>

	        <tr valign="top">
	        <th scope="row">Downloads Page Description</th>
	        <td><textarea class = "regular-textarea" name="es_description_downloads" style = "width: 350px; height: 100px;"><?php echo esc_textarea( get_option('es_description_downloads') ); ?></textarea></td>
	        </tr>
	        
	       	<tr valign = "top">
			    <th scope="row">Downloads Page Header Image</th>
			    <td>
			    	<input type="text" name="es_bg_image_downloads" value = "<?php echo esc_attr(get_option('es_bg_image_downloads')); ?>" id="es_bg_image_downloads" class="regular-text">
			    	<input rel = "es_bg_image_downloads" type="button" name="upload-btn-1" id="upload-btn-1" class="button-secondary image-upload-button" value="Upload Image">
			    	<input rel = "es_bg_image_downloads" type="button" name="reset-btn-1" id="reset-btn-1" class="button-secondary image-reset-button" value="Reset Image">
			    </td>
			</tr>

			<tr valign = "top">
			    <th scope="row">Downloads Page Icon Image</th>
			    <td>
			    	<input type="text" name="es_icon_image_downloads" value = "<?php echo esc_attr(get_option('es_icon_image_downloads')); ?>" id="es_icon_image_downloads" class="regular-text">
			    	<input rel = "es_icon_image_downloads" type="button" name="upload-btn-1" id="upload-btn-1" class="button-secondary image-upload-button" value="Upload Image">
			    	<input rel = "es_icon_image_downloads" type="button" name="reset-btn-1" id="reset-btn-1" class="button-secondary image-reset-button" value="Reset Image">
			    </td>
			</tr>

	    </table>

	    <h3>Welcome Audio</h3>
	    <table class="form-table">
	        
	        <tr valign="top">
	        <th scope="row">Welcome Post Title</th>
	        <td><input class = "regular-text" type="text" name="es_welcome_title" value="<?php echo esc_attr( get_option('es_welcome_title') ); ?>" /></td>
	        </tr>

	        <tr valign="top">
	        <th scope="row">Welcome Post "Category" (shows in the player area)</th>
	        <td><textarea class = "regular-textarea" name="es_welcome_cat" style = "width: 350px; height: 100px;"><?php echo esc_textarea( get_option('es_welcome_cat') ); ?></textarea></td>
	        </tr>
	        
	       	<tr valign = "top">
			    <th scope="row">Welcome Post Audio</th>
			    <td>
			    	<input type="text" name="es_welcome_stream" value = "<?php echo esc_attr(get_option('es_welcome_stream')); ?>" id="es_welcome_stream" class="regular-text">
			    	<input rel = "es_welcome_stream" type="button" name="upload-btn-1" id="upload-btn-1" class="button-secondary image-upload-button" value="Upload MP3">
			    	<input rel = "es_welcome_stream" type="button" name="reset-btn-1" id="reset-btn-1" class="button-secondary image-reset-button" value="Reset MP3">
			    </td>
			</tr>

			<tr valign = "top">
			    <th scope="row">Welcome Post Cover Image</th>
			    <td>
			    	<input type="text" name="es_welcome_cover_image" value = "<?php echo esc_attr(get_option('es_welcome_cover_image')); ?>" id="es_welcome_cover_image" class="regular-text">
			    	<input rel = "es_welcome_cover_image" type="button" name="upload-btn-1" id="upload-btn-1" class="button-secondary image-upload-button" value="Upload Image">
			    	<input rel = "es_welcome_cover_image" type="button" name="reset-btn-1" id="reset-btn-1" class="button-secondary image-reset-button" value="Reset Image">
			    </td>
			</tr>

			<tr valign = "top">
			    <th scope="row">Welcome Post BG Image</th>
			    <td>
			    	<input type="text" name="es_welcome_bg_image" value = "<?php echo esc_attr(get_option('es_welcome_bg_image')); ?>" id="es_welcome_bg_image" class="regular-text">
			    	<input rel = "es_welcome_bg_image" type="button" name="upload-btn-1" id="upload-btn-1" class="button-secondary image-upload-button" value="Upload Image">
			    	<input rel = "es_welcome_bg_image" type="button" name="reset-btn-1" id="reset-btn-1" class="button-secondary image-reset-button" value="Reset Image">
			    </td>
			</tr>

			<tr valign="top">
	        <th scope="row">Welcome Post Duration</th>
	        <td><input class = "regular-text" type="text" name="es_welcome_duration" value="<?php echo esc_attr( get_option('es_welcome_duration') ); ?>" /></td>
	        </tr>

	    </table>

	    <a href = "Javascript:;" id = "toggle-es-audio-posts-div">Show Audio Posts Fields</a>

	    <div id = "es-audio-posts-div" style = "display: none;">
	    	<?php
	    		
	    		//update_option("es_audio_posts", false);

	    		if(!get_option("es_audio_posts")) {
		    		print "Setting up placeholder audio post...<br />";
		    		
		    		$items = "";

		    		$item['title'] = "Audio Post Title";
		    		$item['cat'] = "Audio Blog Post";
		    		$item['cover'] = "";
		    		$item['bg'] = "";
		    		$item['stream'] = "";
		    		$item['visible'] = "false";
		    		$item['duration'] = "9:99";

		    		$items[] = $item; // set up dummy post
		    		
		    		update_option("es_audio_posts", $items);

					//print "First audio post set: <br />";

					?><pre><?php print_r(get_option("es_audio_posts")); ?></pre><?php

		    	} 

		    	$audio_posts = get_option("es_audio_posts");

		    	$final_audio_posts = array();

		    	foreach($audio_posts as $key=>$value) {
		    		if($audio_posts[$key]['delete'] == "delete") {
		    			//print "Deleting audio post $key...<br />";
		    		} else {
		    			$final_audio_posts[] = $audio_posts[$key];
		    		}
		    	}

		    	update_option("es_audio_posts", $final_audio_posts);

		    	$audio_posts = get_option("es_audio_posts");

		    	$numAudioPosts = 0;
		    	
		    	foreach($audio_posts as $key=>$value) {
					$numAudioPosts++;

					?><h2>Audio Post: <?php print $key ?></h2><table class="form-table"><?php

					if(!isset($audio_posts[$key]['duration'])) {
		    			$audio_posts[$key]['duration'] = "9:99";
		    		}

		    		foreach($audio_posts[$key] as $item_key=>$item_value) {
		    			$row_id = "audio_item_".$key."_".$item_key;
		    			if($item_key == "delete") {
		    				continue;
		    			}
		    			?>
				        <tr valign="top">
					        <th scope="row"><?php print $item_key ?></th>
					        <td>
					        	<?php if($item_key == "visible") { ?>
					        	<select name = "es_audio_posts[<?php print $key ?>][<?php print $item_key ?>]">
					        		<option value = "true" <?php print ($item_value == "true") ? "selected" : ""; ?>>True</option>
					        		<option value = "false" <?php print ($item_value == "false") ? "selected" : ""; ?>>False</option>
					        	</select>
					        	<?php } else { ?>
				    			<input id = "<?php print $row_id ?>" class = "regular-text" type="text" name="es_audio_posts[<?php print $key ?>][<?php print $item_key ?>]" value="<?php echo esc_attr($item_value); ?>" />
				    			<?php } ?>
					        	<?php if($item_key == "cover" || $item_key == "bg") { ?>
					        	<input rel = "<?php print $row_id ?>" type="button" name="upload-btn-1" id="upload-btn-1" class="button-secondary image-upload-button" value="Upload Image">
				    			<input rel = "<?php print $row_id ?>" type="button" name="reset-btn-1" id="reset-btn-1" class="button-secondary image-reset-button" value="Reset Image">
				    			<?php } ?>
				    			<?php if($item_key == "stream") { ?>
					        	<input rel = "<?php print $row_id ?>" type="button" name="upload-btn-1" id="upload-btn-1" class="button-secondary image-upload-button" value="Upload MP3">
				    			<input rel = "<?php print $row_id ?>" type="button" name="reset-btn-1" id="reset-btn-1" class="button-secondary image-reset-button" value="Reset MP3">
				    			<?php } ?>

					        </td>
				        </tr>
				        <?php
		    		}
		    		?><tr valign = "top">
				        	<th scope = "row">(Delete entry?)</th>
				        	<td><input name = "es_audio_posts[<?php print $key ?>][delete]" value = "delete" type = "checkbox" /></td>
				        </tr><?php
		    		?></table><?php
				}
	    	?>

	    	<div id = "audioPostsFieldWrapper"></div>

	    	<br />
	    	<input type = "button" class = "button-secondary" value = "Add A New Audio Post" id = "add-audio-fields" />

	    </div>

	    <br /><br />

	    <a href = "Javascript:;" id = "toggle-es-mappings-div">Show Download Mapping Fields</a>

	    <div id = "es-mappings-div" style = "display: none;">

		    <?php
		    	
		    	// uncomment to reset the mappings!
		    	// update_option("es_mappings", false);

		    	if(!get_option("es_mappings")) {
		    		print "Setting up mappings...<br />";
		    		
		    		set_default_mappings();

					print "First mappings set: <br />";

					?><pre><?php print_r(get_option("es_mappings")); ?></pre><?php

		    	} 

		    	$mappings = get_option("es_mappings");

		    	$numMappings = 0;
		    	
		    	foreach($mappings as $key=>$value) {
					$numMappings++;
		    		?><h2>Stream => Download Mappings: <?php print $key ?></h2><table class="form-table"><?php
		    		
		    		if(!isset($mappings[$key]['duration'])) {
		    			$mappings[$key]['duration'] = "9:99";
		    		}
		    		
		    		foreach($mappings[$key] as $item_key=>$item_value) {
		    			$row_id = "item_".$key."_".$item_key;
		    			?>
				        <tr valign="top">
					        <th scope="row"><?php print $item_key ?></th>
					        <td>
					        	<input id = "<?php print $row_id ?>" class = "regular-text" type="text" name="es_mappings[<?php print $key ?>][<?php print $item_key ?>]" value="<?php echo esc_attr($item_value); ?>" />
					        	<?php if($item_key == "cover" || $item_key == "bg") { ?>
					        	<input rel = "<?php print $row_id ?>" type="button" name="upload-btn-1" id="upload-btn-1" class="button-secondary image-upload-button" value="Upload Image">
				    			<input rel = "<?php print $row_id ?>" type="button" name="reset-btn-1" id="reset-btn-1" class="button-secondary image-reset-button" value="Reset Image">
				    			<?php } ?>
					        </td>
				        </tr>
			    		<?php
		    		}

		    		?></table><?php
		    		
		    	}


		    ?>

		    <div id = "fieldWrapper"></div>

		    <input type = "button" class = "button-secondary" value = "Add a New Mapping" id = "add-fields" />

		</div><!-- es_mappings_div -->
	    <?php submit_button(); ?>

	    <?php
		    // jQuery
			wp_enqueue_script('jquery');
			// This will enqueue the Media Uploader script
			wp_enqueue_media();
		?>

	    <script>
	    	
	    	// form javascript!

	    	var numMappings = <?php print $numMappings - 1 ?>;
	    	var numAudioPosts = <?php print $numAudioPosts -1 ?>;

		    jQuery(document).ready(function($) {
		    	
		    	jQuery('.image-reset-button').click(function(e) {
					e.preventDefault();
					var rel = jQuery(this).attr("rel");
			    	var target = jQuery("#" + rel);
					target.val("");
				});

			    jQuery('.image-upload-button').click(function(e) {
			    	e.preventDefault();
			        var rel = jQuery(this).attr("rel");
			    	var target = jQuery("#" + rel);
					var image = wp.media({ 
			            title: 'Upload Image',
			            // mutiple: true if you want to upload multiple files at once
			            multiple: false
			        }).open()
			        .on('select', function(e){
			            // This will return the selected image from the Media Uploader, the result is an object
			            var uploaded_image = image.state().get('selection').first();
			            // We convert uploaded_image to a JSON object to make accessing it easier
			            // Output to the console uploaded_image
			            console.log(uploaded_image);
			            var image_url = uploaded_image.toJSON().url;
			            // Let's assign the url value to the input field
			            target.val(image_url);
			        });
			    });

		    	jQuery('#add-fields').click(function(e) {
		    		e.preventDefault();
		    		numMappings++;
		    		var myBlankForm = '<h2>Download Item Mappings: New Item (' + numMappings + ')</h2><table class="form-table"> <tr valign="top"> <th scope="row">wp_id</th> <td> <input class="regular-text" type="text" name="es_mappings[' + numMappings + '][wp_id]" value="1470"/> </td></tr><tr valign="top"> <th scope="row">title</th> <td> <input class="regular-text" type="text" name="es_mappings[' + numMappings + '][title]" value="Affinity"/> </td></tr><tr valign="top"> <th scope="row">description</th> <td> <input class="regular-text" type="text" name="es_mappings[' + numMappings + '][description]" value="Lorem ipsum"/> </td></tr><tr valign="top"> <th scope="row">cover</th> <td> <input class="regular-text" type="text" name="es_mappings[' + numMappings + '][cover]" value="Affinity-200.jpg"/> </td></tr><tr valign="top"> <th scope="row">page-link</th> <td> <input class="regular-text" type="text" name="es_mappings[' + numMappings + '][page-link]" value="#"/> </td></tr><tr valign="top"> <th scope="row">bg</th> <td> <input class="regular-text" type="text" name="es_mappings[' + numMappings + '][bg]" value="Affinity.jpg"/> </td></tr><tr valign="top"> <th scope="row">stream_link</th> <td> <input class="regular-text" type="text" name="es_mappings[' + numMappings + '][stream_link]" value="https://s3-us-west-2.amazonaws.com/hqsstream/White+Light+5+stream+192.mp3"/> </td></tr><tr valign="top"> <th scope="row">category</th> <td> <input class="regular-text" type="text" name="es_mappings[' + numMappings + '][category]" value="e"/> </td></tr></table>';
		    		jQuery('#fieldWrapper').append(myBlankForm);
		    	});

				jQuery('#add-audio-fields').click(function(e) {
		    		e.preventDefault();
		    		numAudioPosts++;
		    		//alert("Clicked");
		    		var myBlankForm = '<h2>Audio Post: New Item (' + numAudioPosts + ')</h2><table class="form-table"> <tr valign="top"> <th scope="row">title</th> <td> <input id="audio_item' + numAudioPosts + 'title" class="regular-text" type="text" name="es_audio_posts[' + numAudioPosts + '][title]" value="Audio Post Title"/> </td></tr><tr valign="top"> <th scope="row">cat</th> <td> <input id="audio_item' + numAudioPosts + 'cat" class="regular-text" type="text" name="es_audio_posts[' + numAudioPosts + '][cat]" value="Audio Blog Post"/> </td></tr><tr valign="top"> <th scope="row">cover</th> <td> <input id="audio_item' + numAudioPosts + 'cover" class="regular-text" type="text" name="es_audio_posts[' + numAudioPosts + '][cover]" value=""/> </td></tr><tr valign="top"> <th scope="row">bg</th> <td> <input id="audio_item' + numAudioPosts + 'bg" class="regular-text" type="text" name="es_audio_posts[' + numAudioPosts + '][bg]" value=""/> </td></tr><tr valign="top"> <th scope="row">stream</th> <td> <input id="audio_item' + numAudioPosts + 'stream" class="regular-text" type="text" name="es_audio_posts[' + numAudioPosts + '][stream]" value=""/> </td></tr><tr valign="top"> <th scope="row">visible</th> <td> <select name="es_audio_posts[' + numAudioPosts + '][visible]"> <option value="true">True</option> <option value="false" selected>False</option> </select> </td></tr></table>';
		    		jQuery('#audioPostsFieldWrapper').append(myBlankForm);
		    	});

				jQuery('#toggle-es-mappings-div').click(function() {
					var button = $(this);
					$('#es-mappings-div').slideToggle(function() {
						if($('#es-mappings-div').is(":visible")) {
							button.html("Hide Download Mapping Fields")
						} else {
							button.html("Show Download Mapping Fields");
						}						
					});
				});

				jQuery('#toggle-es-audio-posts-div').click(function() {
					var button = $(this);
					$('#es-audio-posts-div').slideToggle(function() {
						if($('#es-audio-posts-div').is(":visible")) {
							button.html("Hide Audio Posts Fields")
						} else {
							button.html("Show Audio Posts Fields");
						}						
					});
				});
		    });
		    	
	    </script>
	    

	</form>
	</div>
	<?php
}

function es_plugin_create_admin_menu() {
	// add menu 
	add_options_page('Easy Streamer Options', 'Easy Streamer', 'manage_options', 'es-plugin-admin', 'es_plugin_options');
	// initialise settings
	add_action('admin_init', 'es_plugin_register_settings');
}

add_action( 'admin_menu', 'es_plugin_create_admin_menu' );

function set_default_mappings() {
	$mapping = "";

	$mapping[] = array(
		"wp_id" => 1470,
		"title" => "Affinity",
		"description" => "Soul mate meditation",
		"cover" => "Affinity-200.jpg",
		"page-link" => "#",
		"bg" => "Affinity.jpg",
		"stream_link" => "",
		"category" => "i"
	);

	$mapping[] = array(
		"wp_id" => 1505,
		"title" => "Body Relaxation",
		"description" => "Mindful body scan",
		"cover" => "",
		"page-link" => "#",
		"bg" => "",
		"stream_link" => "",
		"category" => "e"
	);

	$mapping[] = array(
		"wp_id" => 1507,
		"title" => "Chakra Cleanse",
		"description" => "Meditation for energy balance",
		"cover" => "",
		"page-link" => "#",
		"bg" => "",
		"stream_link" => "",
		"category" => "e"
	);

	$mapping[] = array(
		"wp_id" => 1464,
		"title" => "Corinthians",
		"description" => "A reading of the Love Chapter",
		"cover" => "",
		"page-link" => "#",
		"bg" => "",
		"stream_link" => "",
		"category" => "e"
	);

	$mapping[] = array(
		"wp_id" => 8740,
		"title" => "Eternal Flame",
		"description" => "Guided spiritual awakening ",
		"cover" => "",
		"page-link" => "#",
		"bg" => "",
		"stream_link" => "",
		"category" => "e"
	);

	$mapping[] = array(
		"wp_id" => 1442,
		"title" => "Hsin Hsin Ming",
		"description" => "Verses on the Faith Mind reading",
		"cover" => "",
		"page-link" => "#",
		"bg" => "",
		"stream_link" => "",
		"category" => "e"
	);

	$mapping[] = array(
		"wp_id" => 1476,
		"title" => "Immersion",
		"description" => "Journey meditation",
		"cover" => "",
		"page-link" => "#",
		"bg" => "",
		"stream_link" => "",
		"category" => "m"
	);

	$mapping[] = array(
		"wp_id" => 2004,
		"title" => "Infinity Loop",
		"description" => "Stress & anger management",
		"cover" => "",
		"page-link" => "#",
		"bg" => "",
		"stream_link" => "",
		"category" => "e"
	);

	$mapping[] = array(
		"wp_id" => 1472,
		"title" => "Lift",
		"description" => "Heart-centered forgiveness",
		"cover" => "",
		"page-link" => "#",
		"bg" => "",
		"stream_link" => "",
		"category" => "i"
	);

	$mapping[] = array(
		"wp_id" => 2949,
		"title" => "Lionheart",
		"description" => "Courage & confidence",
		"cover" => "",
		"page-link" => "#",
		"bg" => "",
		"stream_link" => "",
		"category" => "m"
	);

	$mapping[] = array(
		"wp_id" => 1515,
		"title" => "One Amen One",
		"description" => "Breath counting with Amen mantra",
		"cover" => "",
		"page-link" => "#",
		"bg" => "",
		"stream_link" => "",
		"category" => "e"
	);

	$mapping[] = array(
		"wp_id" => 1517,
		"title" => "One Love One",
		"description" => "Breath counting with Love mantra",
		"cover" => "",
		"page-link" => "#",
		"bg" => "",
		"stream_link" => "",
		"category" => "e"
	);

	$mapping[] = array(
		"wp_id" => 1519,
		"title" => "One Sat Nam One",
		"description" => "Breath counting with Sat Nam mantra",
		"cover" => "",
		"page-link" => "#",
		"bg" => "",
		"stream_link" => "",
		"category" => "e"
	);

	$mapping[] = array(
		"wp_id" => 1511,
		"title" => "Portal",
		"description" => "Cosmic insight meditation",
		"cover" => "",
		"page-link" => "#",
		"bg" => "",
		"stream_link" => "",
		"category" => "m"
	);

	$mapping[] = array(
		"wp_id" => 6972,
		"title" => "The Purist",
		"description" => "Essential meditation background",
		"cover" => "",
		"page-link" => "#",
		"bg" => "",
		"stream_link" => "",
		"category" => "m"
	);

	$mapping[] = array(
		"wp_id" => 1474,
		"title" => "The Stilling Heart",
		"description" => "Embrace the Unknowable",
		"cover" => "",
		"page-link" => "#",
		"bg" => "",
		"stream_link" => "",
		"category" => "i"
	);

	$mapping[] = array(
		"wp_id" => 1509,
		"title" => "To A Further Shore",
		"description" => "Ocean waves to calm worry ",
		"cover" => "",
		"page-link" => "#",
		"bg" => "",
		"stream_link" => "",
		"category" => "i"
	);

	$mapping[] = array(
		"wp_id" => 4224,
		"title" => "Transfiguration",
		"description" => "Open soul meditation",
		"cover" => "",
		"page-link" => "#",
		"bg" => "",
		"stream_link" => "",
		"category" => "m"
	);

	$mapping[] = array(
		"wp_id" => 1478,
		"title" => "Tuning",
		"description" => "Brain waves meditation",
		"cover" => "",
		"page-link" => "#",
		"bg" => "",
		"stream_link" => "",
		"category" => "m"
	);

	$mapping[] = array(
		"wp_id" => 1513,
		"title" => "White Light",
		"description" => "Energy renewal visualization",
		"cover" => "",
		"page-link" => "#",
		"bg" => "",
		"stream_link" => "",
		"category" => "e"
	);

	$mapping[] = array(
		"wp_id" => 6808,
		"title" => "Winter Solstice Meditation",
		"description" => "True essence visualization",
		"cover" => "",
		"page-link" => "#",
		"bg" => "",
		"stream_link" => "",
		"category" => "e"
	);

	update_option("es_mappings", false);
	update_option("es_mappings", $mapping);
}

/* Can be removed after testing has finished */



function hybrid_streamer() {

	$purchases = edd_get_users_purchases(get_current_user_id(), 20, true, 'any');

	if ($purchases) :
		foreach ( $purchases as $payment ) :
			$downloads      = edd_get_payment_meta_cart_details( $payment->ID, true );
			$purchase_data  = edd_get_payment_meta( $payment->ID );
			$email          = edd_get_payment_user_email( $payment->ID );

			if ( $downloads ) :
				foreach ( $downloads as $download ) :

					// Skip over Bundles. Products included with a bundle will be displayed individually
					if ( edd_is_bundled_product( $download['id'] ) )
						continue; 

					$price_id 		= edd_get_cart_item_price_id( $download );
					$download_files = edd_get_download_files( $download['id'], $price_id );
					$name           = get_the_title( $download['id'] );
					$post_id 		= $download['id'];
					$category 		= get_the_category($post_id);
					
					$download_url = edd_get_download_file_url( $purchase_data['key'], $email, $filekey, $download['id'], $price_id );
										
					$stream_assets = get_mapped_item($post_id);
					$stream_assets['stream_link'] = $download_url;
					if($stream_assets !== false) {
						?><pre><?php print_r($stream_assets); ?></pre><?php
					}

					// Retrieve and append the price option name
					if ( ! empty( $price_id ) ) {
						$name .= ' - ' . edd_get_price_option_name( $download['id'], $price_id, $payment->ID );
					}

				endforeach; // End foreach $downloads
			endif; // End if $downloads
		endforeach;
	
	else : 

		if(!is_user_logged_in()) {
			?><p class="edd-no-downloads">Please <a href = "<?php print get_bloginfo('wpurl') ?>/customer-login/">log in</a> to view your meditations.</p><?php
		} else {
			?><p class="edd-no-downloads"><?php _e( 'You have not purchased any downloads', 'easy-digital-downloads' ); ?></p><?php
		}
	
	endif;
}

add_shortcode('hybrid_streamer_page', 'hybrid_streamer');