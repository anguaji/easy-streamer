# Easy-streamer.php
## Function map for the plugin file - so I don't get lost!

### About The Plugin
The Easy-Streamer plugin allows a streaming page to be set up linking a user's purchases from EDD. The plugin depends on the EDD plugin being installed first. The main pages are called via shortcodes, and the appearence options are set up in the admin settings (Settings > Easy Streamer)

### Shortcodes
```[streaming_page_full]``` 

Shortcode to bring up the streaming page

```[streaming_downloads_page]``` 

Shortcode to bring up the downloads page

### Function Reference

``` easy_streamer_activate()```
// register_activation_hook(__FILE__, "easy_streamer_activate");

``` easy_streamer_deactivate()```
// register_deactivation_hook(__FILE__, "easy_streamer_deactivate");

``` easy_streamer_call()```
// add_shortcode('easy_streamer_page', 'easy_streamer_call');
// DEBUG: main streaming page shortcode function - depends on EDD hooks

``` get_streams()```
// library function to retrieve streams as an array

```$ES_STATIC_ID;``` // tracks whether the shortcode has been called.```

``` streaming_page_full()```
// add_shortcode('streaming_page_full', 'streaming_page_full');
// main streaming page call

``` streaming_downloads_page()```
// add_shortcode('streaming_page_downloads', 'streaming_downloads_page');
// main download page call

``` easy_streamer_setup_js()```
// add_action('wp_footer', 'easy_streamer_setup_js');
// if shortcode is used, enque styles and js

``` easy_streamer_page_getter(```page_template)```
// add_filter( 'page_template', 'easy_streamer_page_getter' );
// custom page template - may not be needed! May be controlled in admin options

``` es_plugin_register_settings()```
// standalone function registers the settings for the plugin.

``` load_es_wp_media_files()```
// add_action('admin_enqueue_scripts', 'load_es_wp_media_files');
// adds the media uploader in the admin section on the fly.

``` get_mapped_item(```wp_id)```
// library function to retrieve a mapping for a download

``` es_plugin_options()```
// standalone function sets up the admin page

``` es_plugin_create_admin_menu()```
// add_action( 'admin_menu', 'es_plugin_create_admin_menu' );
// adds an options page
// calls es_plugin_options
// calls es_plugin_register_settings

``` set_default_mappings()```
// standalone functions sets default mappings of downloads

``` hybrid_streamer()```
// DEBUG: add_shortcode('hybrid_streamer_page', 'hybrid_streamer');