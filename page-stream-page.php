<?php /* Template Name: Stream Full Width Page */ ?><?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @since 1.0.0
 */
//get_header();
?><?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <main>
 * and the left sidebar conditional
 *
 * @since 1.0.0
 */
?><?php get_header(); ?>


	<?php
	while ( have_posts() ) : the_post();
		the_content( __( 'Read more', 'arcade') );
		get_template_part( 'content', 'footer' );
		comments_template( '', true );
	endwhile;
	?>
	
<?php get_footer(); ?>